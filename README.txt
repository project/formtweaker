Download and untar into your sites/all/modules directory.

Enable the module under Admin > Site Building > Modules

Enable the modified selection behavior for a certain content type by going to
Administer > Content Management > Content Types 
and edit the content type. 
Near the bottom of the content type edit form, you can choose the default selection behaviour for all vacabularies that apply to the content type.  
You can also choose to over-ride that default behaviour for any individual vocabulary.

